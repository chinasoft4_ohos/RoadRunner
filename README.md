﻿# RoadRunner

#### 项目介绍
- 项目名称：RoadRunner
- 所属系列：openharmony的第三方组件适配移植
- 功能：主要实现使用SVG图像制作自己的加载动画。
- 项目移植状态：主功能完成
- 调用差异：原库引用外部组件，该库使用自定义新增ColorSeekBar，用于着色器扩展，保证功能一致。
- 开发版本：sdk6,DevEco Studio 2.2 Beta1
- 基线版本：master分支

#### 效果演示
HomeView

<img src="gif/HomeView.gif"></img>

DragView

<img src="gif/DragView.gif"></img>

DeterminateView

<img src="gif/DeterminateView.gif"></img>

MaterialView

<img src="gif/MaterialView.gif"></img>

TwoWay

<img src="gif/TwoWay.gif"></img>

Progress

<img src="gif/Progress.gif"></img>
#### 安装教程

1.在项目根目录下的build.gradle文件中添加

       allprojects {
          repositories {
              maven {
                  url 'https://s01.oss.sonatype.org/content/repositories/releases/'
               }
            }
        }

2.在entry模块的build.gradle文件中添加maven仓库依赖

    dependencies {
        implementation 'com.gitee.chinasoft_ohos:roadrunner:1.0.0'
        implementation 'com.gitee.chinasoft_ohos:colorseekbar:1.0.0'
    }


在sdk6，DevEco Studio 2.2 Beta1 下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1.制作svg数据，数据格式如下所示

     M306.314,58.9801 C275.235,27.9011,224.837,27.9011,193.759,58.9801
    L39.0019,213.736 C15.6832,237.055,15.6832,274.838,39.0019,298.158
    C58.2219,317.378,87.2116,320.482,109.874,308.007
    C112.241,307.888,114.569,306.993,116.38,305.202 L271.136,150.445
    C286.675,134.906,286.675,109.717,271.136,94.1779
    C255.597,78.6389,230.408,78.6389,214.869,94.1779 L88.2461,220.8
    C84.366,224.68,84.366,230.987,88.2461,234.866
    C92.1263,238.746,98.4335,238.746,102.313,234.866 L228.935,108.245
    C236.715,100.465,249.309,100.465,257.07,108.245
    C264.85,116.025,264.85,128.619,257.07,136.379 L109.337,284.111
    C93.7979,299.65,68.6085,299.65,53.0694,284.111
    C37.5304,268.572,37.5304,243.383,53.0694,227.844 L207.825,73.0468
    C231.144,49.7281,268.928,49.7281,292.247,73.0468
    C315.566,96.3654,315.566,134.149,292.247,157.469 L151.558,298.158
    C147.678,302.038,147.678,308.345,151.558,312.225
    C155.438,316.105,161.745,316.105,165.625,312.225 L306.314,171.535
    C337.393,140.457,337.393,90.0591,306.314,58.98 Z

2.引用view

  (1)Two way

       <com.github.glomadrian.roadrunner.IndeterminateRoadRunner
             ohos:id="$+id:two_way"
             ohos:height="match_parent"
             ohos:width="match_parent"
             ohos:above="$id:frameLayout"
             ohos:align_parent_end="true"
             ohos:align_parent_right="true"
             app:left_line_animation_start_delay="2500"
             app:left_line_animation_time="2800"
             app:left_line_max_size="0.5"
             app:line_size="0.05"
             app:movement_direction="1"
             app:movement_loop_time="4000"
             app:path_animation_type="1"
             app:path_color="$string:black"
             app:path_data="$string:github"
             app:path_original_height="512"
             app:path_original_width="512"
             app:right_line_animation_start_delay="2000"
             app:right_line_animation_time="2000"
             app:right_line_max_size="0.5"
             app:roadRunner_animate_on_start="true"
             app:stroke_width="10">
     </com.github.glomadrian.roadrunner.IndeterminateRoadRunner>
          
（2）Material

     <com.github.glomadrian.roadrunner.IndeterminateRoadRunner
                ohos:id="$+id:material"
                ohos:height="match_parent"
                ohos:width="match_parent"
                ohos:above="$id:frameLayout"
                ohos:align_parent_end="true"
                ohos:align_parent_right="true"
                app:movement_direction="1"
                app:path_animation_type="0"
                app:path_color="$string:wihte"
                app:path_data="$string:twitter"
                app:path_original_height="610"
                app:path_original_width="610"
                app:roadRunner_animate_on_start="true"
                app:stroke_width="10"
                />
                
                
（3）Determinate Two way

         <com.github.glomadrian.roadrunner.IndeterminateRoadRunner
                 ohos:id="$+id:two_way"
                 ohos:height="match_parent"
                 ohos:width="match_parent"
                 ohos:above="$id:frameLayout"
                 ohos:align_parent_end="true"
                 ohos:align_parent_right="true"
                 app:left_line_animation_start_delay="2500"
                 app:left_line_animation_time="2800"
                 app:left_line_max_size="0.5"
                 app:line_size="0.05"
                 app:movement_direction="1"
                 app:movement_loop_time="4000"
                 app:path_animation_type="1"
                 app:path_color="$string:black"
                 app:path_data="$string:github"
                 app:path_original_height="512"
                 app:path_original_width="512"
                 app:right_line_animation_start_delay="2000"
                 app:right_line_animation_time="2000"
                 app:right_line_max_size="0.5"
                 app:roadRunner_animate_on_start="true"
                 app:stroke_width="10">
     </com.github.glomadrian.roadrunner.IndeterminateRoadRunner>

 Common属性介绍
 
| name                    | type      | info                                                   |
|-------------------------|-----------|--------------------------------------------------------|
| movement_direction      | int       | path移动的类型                                         |
| path_color              | string    | 绘制path的画笔颜色                                     |
| stroke_width            | int       | 画笔宽度                                               |
| path_data               | string    | path的数据                                             |
| path_original_width     | int       | svg动画默认宽度                                        |
| path_original_height    | string    | svg动画默认高度                                        |
| animate_on_start        | boolean   | true or false 初始化view时设置动画是否执行             |

 
 Indeterminate属性介绍
| name                    | type      | info                                                   |
|-------------------------|-----------|--------------------------------------------------------|
| path_animation_type     | int       | 动画类型 可以是:material或者twoWay                     |


 Indeterminate Material没有任何属性设置


 Indeterminate Two way属性介绍
 
| name                         | type              | info                                       |
|------------------------------|-------------------|--------------------------------------------|
| movement_loop_time           | int               | 动画持续时间                               |
| line_size                    | int               | 线条尺寸                                   |
| left_line_animation_time     | int               | 左边线条动画持续时间                       |
| right_line_animation_time    | int               | 右边线条动画持续时间                       |
| left_line_max_size           | int               | 动画中左边线可设置的最大尺寸               |
| right_line_max_size          | int               | 动画中右边线可设置的最大尺寸               |
|left_line_animation_start_delay| int              |左边线条动画开始时间                        |
|right_line_animation_start_delay| int             |右边线条动画开始时间                        |


Determinate属性介绍

| name                     | type      | info                                                   |
|--------------------------|-----------|--------------------------------------------------------|
| min                      | int       | progress最小值                                         |
| max                      | int       | progress最大值                                         |
| movement_loop_time       | int       | 动画持续时间                                           |
| movement_line_size       | int       | 线条尺寸                                               |



#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息
```
Copyright 2016 Adrián García Lomas

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```