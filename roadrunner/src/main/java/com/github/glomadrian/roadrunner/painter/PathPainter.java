package com.github.glomadrian.roadrunner.painter;

import ohos.agp.render.Canvas;

/**
 * Define how a path is painted
 *
 * @author Adrián García Lomas
 */
public interface PathPainter {
  /**
   * start
   */
  void start();
  /**
   * stop
   */
  void stop();
  /**
   * restart
   */
  void restart();

  /**
   * paintPath
   *
   * @param canvas
   */
  void paintPath(Canvas canvas);

  /**
   * setPosition
   *
   * @param position
   */
  void setPosition(float position);

  /**
   * setColor
   *
   * @param color
   */
  void setColor(int color);
}
