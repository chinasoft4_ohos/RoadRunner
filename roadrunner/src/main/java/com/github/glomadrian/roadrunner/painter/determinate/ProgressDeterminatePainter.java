package com.github.glomadrian.roadrunner.painter.determinate;

import com.github.glomadrian.roadrunner.painter.RoadRunnerPainter;
import com.github.glomadrian.roadrunner.painter.configuration.Direction;
import com.github.glomadrian.roadrunner.painter.configuration.indeterminate.MaterialPainterConfiguration;
import com.github.glomadrian.roadrunner.painter.indeterminate.IndeterminatePathPainter;
import com.github.glomadrian.roadrunner.path.PathContainer;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;

/**
 * Created by Yahya Bayramoglu on 08/04/16.
 */
public class ProgressDeterminatePainter extends RoadRunnerPainter
        implements DeterminatePathPainter, IndeterminatePathPainter {

    private AnimatorValue movementAnimator;
    private int movementLoopTime = 3000;
    private float movementLineSize = 0.03f;
    private float sideIncrementSize = 0f;

    /**
     * ProgressDeterminatePainter
     *
     * @param pathData
     * @param view
     * @param twoWayDeterminateConfiguration
     */
    public ProgressDeterminatePainter(PathContainer pathData, Component view,
                                      MaterialPainterConfiguration twoWayDeterminateConfiguration) {
        super(pathData, view);
        initConfiguration(twoWayDeterminateConfiguration);
        init();
    }

    private void initConfiguration(MaterialPainterConfiguration twoWayDeterminateConfiguration) {
        movementDirection = twoWayDeterminateConfiguration.getMovementDirection();
        color = twoWayDeterminateConfiguration.getColor();
        strokeWidth = twoWayDeterminateConfiguration.getStrokeWidth();
    }

    private void init() {
        initPaint();
        initLineMovement();
        initMovementAnimator();
    }

    /**
     * initPaint
     */
    public void initPaint() {
        paint = new Paint();
        paint.setColor(new Color(color));
        paint.setStrokeWidth(strokeWidth);
    }

    /**
     * getPaint
     *
     * @return paint
     */
    public Paint getPaint() {
        return paint;
    }

    private void initLineMovement() {
        movementLinePoints = getNumberOfLinePointsInRange(movementLineSize);
    }

    private void initMovementAnimator() {
        movementAnimator = new AnimatorValue();
        movementAnimator.setDuration(movementLoopTime);
        movementAnimator.setCurveType(Animator.CurveType.LINEAR);
        movementAnimator.setLoopedCount(AnimatorValue.INFINITE);
        movementAnimator.setValueUpdateListener(new MovementLineAnimatorUpdateListener());
    }

    @Override
    public void paintPath(Canvas canvas) {
        System.out.println("sideIncrementSize-------------" + sideIncrementSize);
        if (sideIncrementSize == 0) {
            return;
        }
        int pointInRange = getNumberOfPointsInRange(sideIncrementSize);
        int firstPosition = getPositionForZone(zone) - pointInRange - movementLinePoints;
        int absFirstPosition = Math.abs(firstPosition);

        int right = -movementLinePoints;
        int left;
        /**
         * Somehow when progress is about to 1.f then it crashes by IndexOutOfBoundsException
         * and as i debugged it's because multiplication creating an offset which bigger than pointsSize
         *
         * pointsLeft: 9900
         * fixedPoints: 150.0
         * firstPosition: -10050
         * offset: 10050
         *
         * To prevent that, i calculated the max value
         */
        if (absFirstPosition > pointsNumber) {
            if (firstPosition > 0) {
                left = pointInRange + (absFirstPosition - pointsNumber) + 1;
            } else {
                left = pointInRange - (absFirstPosition - pointsNumber) - 1;
            }
        } else {
            left = pointInRange;
        }

        if (movementDirection.equals(Direction.COUNTER_CLOCKWISE)) {
            int temp = left;
            left = right;
            right = temp;
        }

        drawWithOffset(zone, right, left, movementLinePoints, canvas, paint);
    }

    @Override
    public void setProgress(float value) {
        if (value <= 1F && value >= 0) {
            sideIncrementSize = value;
            view.invalidate();
        }
    }

    @Override
    public void start() {
        System.out.println("start------------------------------");
        movementAnimator.start();
    }

    @Override
    public void stop() {
        movementAnimator.end();
    }

    @Override
    public void restart() {
        movementAnimator.end();
        movementAnimator.start();
    }

    private class MovementLineAnimatorUpdateListener implements AnimatorValue.ValueUpdateListener {
        @Override
        public void onUpdate(AnimatorValue animatorValue, float v) {
            if (movementDirection.equals(Direction.CLOCKWISE)) {
                zone = v;
            } else {
                zone = 1 - v;
            }
            view.invalidate();
        }
    }
}
