package com.github.glomadrian.roadrunner.painter.configuration.factory;

import com.github.glomadrian.roadrunner.builder.RoadRunnerBuilder;
import com.github.glomadrian.roadrunner.painter.configuration.Direction;
import com.github.glomadrian.roadrunner.painter.configuration.PathPainterConfiguration;
import com.github.glomadrian.roadrunner.painter.configuration.determinate.TwoWayDeterminateConfiguration;
import com.github.glomadrian.roadrunner.painter.configuration.indeterminate.MaterialPainterConfiguration;
import com.github.glomadrian.roadrunner.painter.configuration.indeterminate.TwoWayIndeterminateConfiguration;
import com.github.glomadrian.roadrunner.painter.determinate.DeterminatePainter;
import com.github.glomadrian.roadrunner.painter.indeterminate.IndeterminatePainter;
import ohos.agp.components.AttrSet;
import ohos.agp.utils.Color;

/**
 * @author Adrián García Lomas
 */
public class PathPainterConfigurationFactory {
    /**
     * makeConfiguration
     *
     * @param builder
     * @param indeterminatePainter
     * @return makeMaterialConfiguration(builder)
     */
    public static PathPainterConfiguration makeConfiguration(RoadRunnerBuilder builder,
                                                             DeterminatePainter indeterminatePainter) {
        switch (indeterminatePainter) {
            case PROGRESS:
            default:
                return makeMaterialConfiguration(builder);
        }
    }

    /**
     * makeConfiguration
     *
     * @param typedArray
     * @param indeterminatePainter
     * @return makeTwoWayConfiguration(typedArray)
     */
    public static PathPainterConfiguration makeConfiguration(AttrSet typedArray,
                                                             IndeterminatePainter indeterminatePainter) {
        switch (indeterminatePainter) {
            case TWO_WAY:
                return makeTwoWayConfiguration(typedArray);
            case MATERIAL:
                return makeMaterialConfiguration(typedArray);
            default:
                return makeTwoWayConfiguration(typedArray);
        }
    }

    /**
     * makeConfiguration
     *
     * @param typedArray
     * @param indeterminatePainter
     * @return makeProgressDeterminateConfiguration(typedArray)
     */
    public static PathPainterConfiguration makeConfiguration(AttrSet typedArray,
                                                             DeterminatePainter indeterminatePainter) {
        switch (indeterminatePainter) {
            case PROGRESS:
                return makeProgressDeterminateConfiguration(typedArray);
            case TWO_WAY:
            default:
                return makeTwoWayDeterminateConfiguration(typedArray);
        }
    }

    private static PathPainterConfiguration makeMaterialConfiguration(RoadRunnerBuilder builder) {
        MaterialPainterConfiguration materialPainterConfiguration =
                MaterialPainterConfiguration.newBuilder()
                        .withColor(builder.color)
                        .withStrokeWidth(builder.strokeWidth)
                        .withMovementDirection(builder.movementDirection)
                        .build();
        return materialPainterConfiguration;
    }

    private static PathPainterConfiguration makeMaterialConfiguration(AttrSet typedArray) {
        String color = typedArray.getAttr("path_color").get().getStringValue();
        int directionValue = typedArray.getAttr("movement_direction").get().getIntegerValue();
        Direction movementDirection = Direction.fromId(directionValue);
        int strokeWidth = typedArray.getAttr("stroke_width").get().getIntegerValue();

        MaterialPainterConfiguration materialPainterConfiguration =
                MaterialPainterConfiguration.newBuilder()
                        .withColor(Color.getIntColor(color))
                        .withStrokeWidth(strokeWidth)
                        .withMovementDirection(movementDirection)
                        .build();

        return materialPainterConfiguration;
    }

    private static PathPainterConfiguration makeProgressDeterminateConfiguration(AttrSet typedArray) {
        String color =  typedArray.getAttr("path_color").get().getStringValue();
        int directionValue = typedArray.getAttr("movement_direction").get().getIntegerValue();
        Direction movementDirection = Direction.fromId(directionValue);
        int strokeWidth = typedArray.getAttr("stroke_width").get().getIntegerValue();
        MaterialPainterConfiguration materialPainterConfiguration =
                MaterialPainterConfiguration.newBuilder()
                        .withColor(Color.getIntColor(color))
                        .withStrokeWidth(strokeWidth)
                        .withMovementDirection(movementDirection)
                        .build();
        return materialPainterConfiguration;
    }

    private static TwoWayIndeterminateConfiguration makeTwoWayConfiguration(AttrSet typedArray) {
        String color =  typedArray.getAttr("path_color").get().getStringValue();
        int directionValue = typedArray.getAttr("movement_direction").get().getIntegerValue();
        Direction movementDirection = Direction.fromId(directionValue);
        // 默认10
        int strokeWidth = typedArray.getAttr("stroke_width").get().getIntegerValue();
        // 默认4000
        int movementLoopTime = typedArray.getAttr("movement_loop_time").get().getIntegerValue();
        // 默认0.05f
        float lineSize = typedArray.getAttr("line_size").get().getFloatValue();
        // 默认0.05f
        int rightLineAnimationTime = typedArray.getAttr("right_line_animation_time").get().getIntegerValue();
        // 默认0.4f
        float rightLineMaxSize = typedArray.getAttr("right_line_max_size").get().getFloatValue();
        // 默认3000
        int rightLineAnimationStartDelay = typedArray.getAttr("right_line_animation_start_delay").get().getIntegerValue();
        // 默认2000
        int leftLineAnimationTime = typedArray.getAttr("left_line_animation_time").get().getIntegerValue();
        // 默认0.5f

        float leftLineMaxSize = typedArray.getAttr("left_line_max_size").get().getFloatValue();
        // 默认1000
        int leftLineAnimationStartDelay = typedArray.getAttr("left_line_animation_start_delay").get().getIntegerValue();

        TwoWayIndeterminateConfiguration twoWayIndeterminateConfiguration =
                TwoWayIndeterminateConfiguration
                        .newBuilder()
                        .withColor(Color.getIntColor(color))
                        .withStrokeWidth(strokeWidth)
                        .withMovementDirection(movementDirection)
                        .withMovementLoopTime(movementLoopTime)
                        .withMovementLineSize(lineSize)
                        .withLeftLineLoopTime(leftLineAnimationTime)
                        .withLeftLineStartDelayTime(leftLineAnimationStartDelay)
                        .withLeftLineMaxSize(leftLineMaxSize)
                        .withRightLineLoopTime(rightLineAnimationTime)
                        .withRightLineMaxSize(rightLineMaxSize)
                        .withRightLineStartDelayTime(rightLineAnimationStartDelay)
                        .build();
        return twoWayIndeterminateConfiguration;
    }

    private static TwoWayDeterminateConfiguration makeTwoWayDeterminateConfiguration(
            AttrSet typedArray) {
        // 默认Color.RED
        String color = typedArray.getAttr("path_color").get().getStringValue();
        // 默认0
        int directionValue = typedArray.getAttr("movement_direction").get().getIntegerValue();
        Direction movementDirection = Direction.fromId(directionValue);
        // 默认10
        int strokeWidth = typedArray.getAttr("stroke_width").get().getIntegerValue();
        // 默认4000
        int movementLoopTime = typedArray.getAttr("movement_loop_time").get().getIntegerValue();
        // 默认0.05f
        float lineSize = typedArray.getAttr("line_size").get().getFloatValue();

        TwoWayDeterminateConfiguration twoWayDeterminateConfiguration =
                TwoWayDeterminateConfiguration.newBuilder()
                        .withColor(Color.getIntColor(color))
                        .withStrokeWidth(strokeWidth)
                        .withMovementDirection(movementDirection)
                        .withMovementLoopTime(movementLoopTime)
                        .withMovementLineSize(lineSize)
                        .build();

        return twoWayDeterminateConfiguration;
    }
}
