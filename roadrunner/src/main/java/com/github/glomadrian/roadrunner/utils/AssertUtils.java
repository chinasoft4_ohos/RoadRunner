package com.github.glomadrian.roadrunner.utils;

/** AssertUtils
 *  the java assert are disabled by default, this utils is a acceptable alternative
 *  to the keyword assert
 *
 * @author Adrián García Lomas
 */
public final class AssertUtils {

  private AssertUtils() {
    //No instances allowed
  }

  /** assertThis
   *
   * @param condition
   * @param errorMsg
   * @param clazz
   */
  public static void assertThis(boolean condition, String errorMsg, Class clazz) {
    if (!condition) {
      throw new AssertionError(errorMsg + "in " + clazz.getName());
    }
  }
}