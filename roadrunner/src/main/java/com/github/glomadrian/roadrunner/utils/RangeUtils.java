package com.github.glomadrian.roadrunner.utils;

/**
 * RangeUtils
 *
 * @author Adrián García Lomas
 * @since 2021-07-22
 */
public class RangeUtils {
    private RangeUtils() {
    }

    /** getIntValueInRange
     *
     * @param fromRangeMin
     * @param fromRangeMax
     * @param toRangeMin
     * @param toRangeMax
     * @param fromRangeValue
     * @return (((fromRangeValue - fromRangeMin) * newRange) / oldRange) + toRangeMin
     */
    public static int getIntValueInRange(int fromRangeMin, int fromRangeMax, int toRangeMin,
                                         int toRangeMax, int fromRangeValue) {
        int oldRange = fromRangeMax - fromRangeMin;
        int newRange = toRangeMax - toRangeMin;
        return (((fromRangeValue - fromRangeMin) * newRange) / oldRange) + toRangeMin;
    }

    /** getFloatValueInRange
     *
     * @param fromRangeMin
     * @param fromRangeMax
     * @param toRangeMin
     * @param toRangeMax
     * @param fromRangeValue
     * @return (((fromRangeValue - fromRangeMin) * newRange) / oldRange) + toRangeMin
     */
    public static float getFloatValueInRange(float fromRangeMin, float fromRangeMax, float toRangeMin,
                                             float toRangeMax, float fromRangeValue) {
        float oldRange = fromRangeMax - fromRangeMin;
        float newRange = toRangeMax - toRangeMin;
        return (((fromRangeValue - fromRangeMin) * newRange) / oldRange) + toRangeMin;
    }
}
