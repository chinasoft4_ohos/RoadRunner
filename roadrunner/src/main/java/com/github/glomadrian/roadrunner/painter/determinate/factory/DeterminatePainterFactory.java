package com.github.glomadrian.roadrunner.painter.determinate.factory;

import com.github.glomadrian.roadrunner.painter.configuration.PathPainterConfiguration;
import com.github.glomadrian.roadrunner.painter.configuration.determinate.TwoWayDeterminateConfiguration;
import com.github.glomadrian.roadrunner.painter.configuration.indeterminate.MaterialPainterConfiguration;
import com.github.glomadrian.roadrunner.painter.determinate.DeterminatePainter;
import com.github.glomadrian.roadrunner.painter.determinate.DeterminatePathPainter;
import com.github.glomadrian.roadrunner.painter.determinate.ProgressDeterminatePainter;
import com.github.glomadrian.roadrunner.painter.determinate.TwoWayDeterminatePainter;
import com.github.glomadrian.roadrunner.path.PathContainer;
import ohos.agp.components.Component;

/**
 * @author Adrián García Lomas
 */
public class DeterminatePainterFactory {
    /**
     * makeIndeterminatePathPainter
     *
     * @param determinatePainter
     * @param pathContainer
     * @param view
     * @param pathPainterConfiguration
     * @return makeProgressDeterminatePainter
     */
    public static DeterminatePathPainter makeIndeterminatePathPainter(
        DeterminatePainter determinatePainter,
        PathContainer pathContainer, Component view, PathPainterConfiguration pathPainterConfiguration) {

        switch (determinatePainter) {
            case PROGRESS:
                return makeProgressDeterminatePainter(pathContainer, view, pathPainterConfiguration);
            case TWO_WAY:
            default:
                return makeTwoWayDeterminatePainter(pathContainer, view, pathPainterConfiguration);
        }
    }

    private static DeterminatePathPainter makeTwoWayDeterminatePainter(PathContainer pathContainer,
                                                                       Component view, PathPainterConfiguration pathPainterConfiguration) {
        return new TwoWayDeterminatePainter(pathContainer, view,
                (TwoWayDeterminateConfiguration) pathPainterConfiguration);
    }

    private static DeterminatePathPainter makeProgressDeterminatePainter(PathContainer pathContainer,
                                                                         Component view, PathPainterConfiguration pathPainterConfiguration) {
        return new ProgressDeterminatePainter(pathContainer, view,
                (MaterialPainterConfiguration) pathPainterConfiguration);
    }
}
