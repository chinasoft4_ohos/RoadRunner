package com.github.glomadrian.roadrunner.painter;

import com.github.glomadrian.roadrunner.path.PathContainer;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PathMeasure;
import java.util.ArrayList;
import java.util.List;

/** PointPathPainter
 * @author Adrián García Lomas
 *
 */
public abstract class PointPathPainter implements PathPainter {

    private static final String TAG = "PointPathPainter";
    protected PathContainer pathData;
    protected Component view;
    protected int pointsNumber = 10000;
    protected int maxLinePoints = pointsNumber / 2;
    protected List<FloatPoint> points;

    /**
     * PointPathPainter
     *
     * @param pathData
     * @param view
     */
  public PointPathPainter(PathContainer pathData, Component view) {
        this.pathData = pathData;
        this.view = view;
        points = obtainPoints(pathData.path);
      }

  /** obtainPoints
   *
   * @param path Given a path obtain the list of points This method make use of pointsNumber size
   * @return floatPoints
   */
  private List<FloatPoint> obtainPoints(Path path) {
    List<FloatPoint> floatPoints = new ArrayList<>();
      for (float i = 0; i < pointsNumber; i++) {
            float fraction = i / pointsNumber;
            float[] cords = getPathCoordinates(path, fraction);
            if (cords[0] != 0 && cords[1] != 0) {
                FloatPoint point = new FloatPoint(cords[0], cords[1]);
                floatPoints.add(point);
      }
    }
    return floatPoints;
  }

  private float[] getPathCoordinates(Path path, float fraction) {
    float aCoordinates[] = { 0f, 0f };
    PathMeasure pm = new PathMeasure(path, false);
    pm.getPosTan(pm.getLength() * fraction, aCoordinates, new float[2]);
    return aCoordinates;
  }

  /** drawWithOffset Paint a line with a offset for right and left
   *
   * @param zone
   * @param pointsRight
   * @param pointsLeft
   * @param fixedPoints
   * @param canvas
   * @param paint
   */
  protected void drawWithOffset(float zone, int pointsRight, int pointsLeft, float fixedPoints,
                                Canvas canvas, Paint paint) {
    int position = getPositionForZone(zone);
    int firstPosition = (int) (position - pointsLeft - fixedPoints);
    int lastPosition = (int) (position + pointsRight + fixedPoints);

    /**
     * If the last position is greater than the limit then draw from 0 to the offset of this limit
     * also change the last point to the last drawable point without offset
     * for
     * the other line painting
     */
    if (lastPosition > pointsNumber - 1 && lastPosition != pointsNumber) {
      int offset = lastPosition - pointsNumber - 1;
      float[] pointsF = getArrayFloat(points.subList(0, offset));
      lastPosition = pointsNumber - 1;
      drawPoints(canvas, pointsF, paint);
    }

    /**
     * If the firs position is negative then paint the negative offset
     */
    if (firstPosition < 0) {
      int offset = Math.abs(firstPosition);
      float[] pointsF =
          getArrayFloat(points.subList((pointsNumber - 1) - offset, pointsNumber - 1));
      drawPoints(canvas, pointsF, paint);
      firstPosition = 0;

    }
    float[] pointsF = getArrayFloat(points.subList(firstPosition, lastPosition));
    drawPoints(canvas, pointsF, paint);
  }

  private void drawPoints(Canvas canvas, float[] pointsF, Paint paint){
    for(int i = 0; i < pointsF.length; i+=2) {
      canvas.drawPoint(pointsF[i], pointsF[i+1], paint);
    }
  }

  /** getPositionForZone
   *
   * @param zone
   * @return (int) (zone * pointsNumber)
   */
  protected int getPositionForZone(float zone) {
    return (int) (zone * pointsNumber);
  }

  /** getArrayFloat
   *
   * @param points Given a list of points obtain a float[] points to be draw using canvas.drawPoints() method
   * @return floats
   */
  protected float[] getArrayFloat(List<FloatPoint> points) {
    float[] floats = new float[points.size() * 2];
    int iList;

    for (int i = 0; i < floats.length; i += 2) {
      iList = i / 2;
      floats[i] = points.get(iList).x;
      floats[i + 1] = points.get(iList).y;
    }
    return floats;
  }

  /** drawPoint Draw a point in canvas
   *
   * @param point
   * @param canvas
   * @param paint
   */
  protected void drawPoint(FloatPoint point, Canvas canvas, Paint paint) {
    canvas.drawPoint(point.x, point.y, paint);
  }

  /** getNumberOfPointsInRange
   *
   * @param range Get the number of points in the given range from 0f to 1f 1f is the complete path
   * @return (int) (range * pointsNumber)
   */
  protected int getNumberOfPointsInRange(float range) {
    return (int) (range * pointsNumber);
  }

    /** getNumberOfLinePointsInRange
     *
     * @param range Get the number of points in the given range from 0f to 1f 1f is the complete path
     * @return (int) (range * maxLinePoints)
     */
  protected int getNumberOfLinePointsInRange(float range) {
    return (int) (range * maxLinePoints);
  }

  protected static class FloatPoint {
    float x;
    float y;

      /**
       * FloatPoint
       *
       * @param x
       * @param y
       */
    public FloatPoint(float x, float y) {
      this.x = x;
      this.y = y;
    }
  }
}
