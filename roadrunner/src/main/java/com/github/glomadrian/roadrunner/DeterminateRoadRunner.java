package com.github.glomadrian.roadrunner;

import com.github.glomadrian.roadrunner.painter.configuration.PathPainterConfiguration;
import com.github.glomadrian.roadrunner.painter.configuration.factory.PathPainterConfigurationFactory;
import com.github.glomadrian.roadrunner.painter.determinate.DeterminatePainter;
import com.github.glomadrian.roadrunner.painter.determinate.DeterminatePathPainter;
import com.github.glomadrian.roadrunner.painter.determinate.factory.DeterminatePainterFactory;
import com.github.glomadrian.roadrunner.path.PathContainer;
import com.github.glomadrian.roadrunner.utils.AssertUtils;
import com.github.glomadrian.roadrunner.utils.RangeUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.app.Context;

import java.text.ParseException;

/**
 * @author Adrián García Lomas
 */
public class DeterminateRoadRunner extends RoadRunner implements Component.DrawTask {
    private static final String TAG = "DeterminateLoadingPath";
    private int originalWidth;
    private int originalHeight;
    private String pathData;
    private PathContainer pathContainer;
    private DeterminatePathPainter twoWayDeterminatePainter;
    private int min = 0;
    private int max = 100;
    private PathPainterConfiguration pathPainterConfiguration;
    private boolean animateOnStart = true;
    private boolean firstDraw = true;
    private int tag;

    /**
     * DeterminateRoadRunner
     *
     * @param context
     */
    public DeterminateRoadRunner(Context context) {
        super(context);
        throw new UnsupportedOperationException("The view can not be created programmatically yet");
    }

    /**
     * DeterminateRoadRunner
     *
     * @param context
     * @param attrs
     */
    public DeterminateRoadRunner(Context context, AttrSet attrs) {
        super(context, attrs);
        initPath(attrs);
        initConfiguration(attrs);
        invalidate();
    }

    /**
     * DeterminateRoadRunner
     *
     * @param context
     * @param attrs
     * @param defStyleAttr
     */
    public DeterminateRoadRunner(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initPath(attrs);
        initConfiguration(attrs);
    }

    @Override
    public void setColor(int color) {
        twoWayDeterminatePainter.setColor(color);
    }

    private void initPath(AttrSet attrs) {
        pathData = attrs.getAttr("path_data").get().getStringValue();
        originalWidth = attrs.getAttr("path_original_width").get().getIntegerValue();
        originalHeight = attrs.getAttr("path_original_height").get().getIntegerValue();
        animateOnStart = attrs.getAttr("animate_on_start").get().getBoolValue();
        System.out.println("initPath-path_original_width :"+animateOnStart);

        AssertUtils.assertThis(pathData != null, "Path data must be defined", this.getClass());
        AssertUtils.assertThis(!pathData.isEmpty(), "Path data must be defined", this.getClass());
        AssertUtils.assertThis(!pathData.equals(""), "Path data must be defined", this.getClass());
        AssertUtils.assertThis(originalWidth > 0, "Original with of the path must be defined",
            this.getClass());
        AssertUtils.assertThis(originalHeight > 0, "Original height of the path must be defined",
            this.getClass());
    }

    private void initConfiguration(AttrSet attrs) {
        min = attrs.getAttr("min").get().getIntegerValue();
        max = attrs.getAttr("max").get().getIntegerValue();
        pathPainterConfiguration =
            PathPainterConfigurationFactory.makeConfiguration(attrs, DeterminatePainter.TWO_WAY);
    }

    /**
     * onSizeChanged
     *
     * @param w
     * @param h
     */
    protected void onSizeChanged(int w, int h) {
        try{
            pathContainer = buildPathData(w, h, pathData, originalWidth, originalHeight);
            initPathPainter();
        } catch (ParseException e) {
          //System.out.println("Path parse exception:" + e);
        }
        if (animateOnStart) {
            twoWayDeterminatePainter.start();
        }
    }

    private void initPathPainter() {
        twoWayDeterminatePainter = DeterminatePainterFactory.makeIndeterminatePathPainter(
        DeterminatePainter.TWO_WAY, pathContainer, this, pathPainterConfiguration);
    }

    @Override
    public void invalidate() {
        super.invalidate();
        addDrawTask(this::onDraw);
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        tag ++;
        if (tag == 1) {
            onSizeChanged(this.getWidth(), this.getHeight());
        }
        if (!firstDraw) {
            twoWayDeterminatePainter.paintPath(canvas);
        } else {
            firstDraw = false;
        }
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    /**
     * setValue
     *
     * @param value
     */
    public void setValue(int value) {
        if (value <= max || value >= min) {
            float progress = RangeUtils.getFloatValueInRange(min, max, 0f, 1f, value);

            if (twoWayDeterminatePainter != null) {
                twoWayDeterminatePainter.setProgress(progress);
            }
            if (value == max) {
                twoWayDeterminatePainter.stop();
            }
        }
    }

    /**
     * setPosition
     *
     * @param position
     */
    public void setPosition(float position) {
        twoWayDeterminatePainter.setPosition(position);
    }

    /**
     * start
     */
    public void start() {
        twoWayDeterminatePainter.start();
    }

    /**
     * stop
     */
    public void stop() {
        twoWayDeterminatePainter.stop();
    }
}
