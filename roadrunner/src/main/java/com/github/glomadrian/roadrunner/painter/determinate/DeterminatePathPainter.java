package com.github.glomadrian.roadrunner.painter.determinate;

import com.github.glomadrian.roadrunner.painter.PathPainter;

/** DeterminatePathPainter
 *
 * @author Adrián García Lomas
 * @since 2021-06-10
 */
public interface DeterminatePathPainter extends PathPainter {
    /** setProgress
     *
     * @param value progress from 0f to 1f
     */
    void setProgress(float value);
}
