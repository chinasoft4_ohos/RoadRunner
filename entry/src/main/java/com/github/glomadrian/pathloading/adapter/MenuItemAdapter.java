/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.glomadrian.pathloading.adapter;

import com.github.glomadrian.pathloading.ResourceTable;
import com.github.glomadrian.pathloading.utils.MenuItem;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.List;

/**
 * MenuItemAdapter
 *
 * @since 2021-06-10
 */
public class MenuItemAdapter extends BaseItemProvider {
    private static final float NUM_TWO = 0.5f;
    private List<MenuItem> mMenuItems;
    private Context mContext;
    private boolean isInit = true;

    /** 构造方法 MenuItemAdapter
     *
     * @param context
     * @param menuItems
     */
    public MenuItemAdapter(Context context, List<MenuItem> menuItems) {
        mContext = context;
        mMenuItems = menuItems;
    }

    @Override
    public int getCount() {
        return mMenuItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mMenuItems.get(position);
    }

    @Override
    public long getItemId(int id) {
        return id;
    }

    @Override
    public Component getComponent(int position, Component component, ComponentContainer componentContainer) {
        Component cpt = component;

        if (cpt == null) {
            cpt = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_menu_list, null, false);
        }

        Component icon = cpt.findComponentById(ResourceTable.Id_menu_icon);
        Text title = (Text) cpt.findComponentById(ResourceTable.Id_title);
        if (isInit && position == 0) {
            isInit = false;
            ShapeElement shapeElement = new ShapeElement();
            shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#FFCCCCCC")));
            cpt.setBackground(shapeElement);
            title.setTextColor(new Color(Color.getIntColor(mContext.getString(ResourceTable.String_textClickColor))));
        } else {
            icon.setAlpha(NUM_TWO);
        }
        cpt.setTag(icon);
        title.setText(mMenuItems.get(position).getmItemName());

        return cpt;
    }
}
