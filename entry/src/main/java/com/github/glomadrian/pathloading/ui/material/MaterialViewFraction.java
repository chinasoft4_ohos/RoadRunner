package com.github.glomadrian.pathloading.ui.material;

import com.github.glomadrian.pathloading.ResourceTable;
import com.github.glomadrian.pathloading.utils.BaseFraction;
import com.github.glomadrian.roadrunner.IndeterminateRoadRunner;

public class MaterialViewFraction extends BaseFraction {
    public static final int TAG = 3;
    @Override
    public int getUiContent() {
        return ResourceTable.Layout_material_view;
    }

    @Override
    public void initComponent() {
    }
    @Override
    public void requestDisallowInterceptTouchEvent(boolean enable) {

    }
}
