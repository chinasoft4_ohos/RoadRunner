/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.glomadrian.pathloading.utils;

/**
 * 菜单栏item
 *
 * @since 2021-06-10
 */
public class MenuItem {
    private String mItemName;

    /**
     * MenuItem构造函数
     *
     * @param itemName
     */
    public MenuItem(String itemName) {
        mItemName = itemName;
    }

    /**
     * 方法getmItemName
     *
     * @return mItemName
     */
    public String getmItemName() {
        return mItemName;
    }
}
