package com.github.glomadrian.pathloading.ui.twoway;

import com.github.glomadrian.pathloading.ColorSeekBar;
import com.github.glomadrian.pathloading.ResourceTable;
import com.github.glomadrian.pathloading.utils.BaseFraction;
import com.github.glomadrian.roadrunner.DeterminateRoadRunner;
import com.github.glomadrian.roadrunner.IndeterminateRoadRunner;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;

public class TwoWayFraction  extends BaseFraction {
    public static final int TAG = 4;
    private IndeterminateRoadRunner indeterminateLoadingPath;
    private ColorSeekBar colorSeekbar;
    @Override
    public int getUiContent() {
        return ResourceTable.Layout_two_way_view;
    }

    @Override
    public void initComponent() {
        indeterminateLoadingPath =  (IndeterminateRoadRunner)mComponent.findComponentById(ResourceTable.Id_two_way);
        colorSeekbar = (ColorSeekBar) mComponent.findComponentById(ResourceTable.Id_colorSeekbar);
        colorSeekbar.setOnColorChangeListener(new ColorSeekBar.OnColorChangeListener() {
            @Override
            public void onColorChangeListener(int var1, int var2, int var3) {
                indeterminateLoadingPath.setColor(colorSeekbar.getColor());
            }
        });
    }

    @Override
    public void requestDisallowInterceptTouchEvent(boolean enable) {

    }
}
