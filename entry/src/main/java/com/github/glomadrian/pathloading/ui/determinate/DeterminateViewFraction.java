package com.github.glomadrian.pathloading.ui.determinate;

import com.github.clans.fab.FloatingActionButton;
import com.github.glomadrian.pathloading.ResourceTable;
import com.github.glomadrian.pathloading.utils.BaseFraction;
import com.github.glomadrian.roadrunner.DeterminateRoadRunner;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

public class DeterminateViewFraction extends BaseFraction implements Component.ClickedListener {
    public static final int TAG = 2;
    private static final int DOWNLOAD_TIME = 5000;
    private AnimatorValue progressAnimator;

    private DeterminateRoadRunner indeterminateLoadingPath;
    private FloatingActionButton fabBtn;
    private Text progress_text;
    @Override
    public int getUiContent() {
        return ResourceTable.Layout_determinate_view;
    }

    @Override
    public void initComponent() {
       indeterminateLoadingPath =  (DeterminateRoadRunner)mComponent.findComponentById(ResourceTable.Id_determinate);
        fabBtn = (FloatingActionButton) mComponent.findComponentById(ResourceTable.Id_fabBtn);
        fabBtn.setClickedListener(this::onClick);
        progress_text = (Text) mComponent.findComponentById(ResourceTable.Id_progress_text);
        progressAnimator = new AnimatorValue();
        progressAnimator.setDuration(DOWNLOAD_TIME);
        progressAnimator.setDelay(1200);
        progressAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                int value =  (int)(v * 100);
                indeterminateLoadingPath.setValue(value);
                progress_text.setText(value +" %");
            }
        });
    }

    @Override
    public void requestDisallowInterceptTouchEvent(boolean enable) {

    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_fabBtn:
                if(progressAnimator.isRunning()){
                    progressAnimator.stop();
                }
                indeterminateLoadingPath.start();
                progressAnimator.start();
                break;
        }
    }


}
