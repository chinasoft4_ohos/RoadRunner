package com.github.glomadrian.pathloading.utils;

/**
 * ex.
 *
 * @since 2021-07-20
 */
public class RangeUtils {
    private RangeUtils() {
    }

    /**
     * 获取getFloatValueInRange
     *
     * @param fromRangeMin
     * @param fromRangeMax
     * @param toRangeMin
     * @param toRangeMax
     * @param fromRangeValue
     * @return getIntValueInRange
     */
    public static int getIntValueInRange(int fromRangeMin, int fromRangeMax, int toRangeMin,
        int toRangeMax, int fromRangeValue) {
        int oldRange = fromRangeMax - fromRangeMin;
        int newRange = toRangeMax - toRangeMin;
        return (((fromRangeValue - fromRangeMin) * newRange) / oldRange) + toRangeMin;
    }

    /** 获取getFloatValueInRange
     *
     * @param fromRangeMin
     * @param fromRangeMax
     * @param toRangeMin
     * @param toRangeMax
     * @param fromRangeValue
     * @return getFloatValueInRange
     */
    public static float getFloatValueInRange(float fromRangeMin, float fromRangeMax, float toRangeMin,
        float toRangeMax, float fromRangeValue) {
        float oldRange = fromRangeMax - fromRangeMin;
        float newRange = toRangeMax - toRangeMin;
        return (((fromRangeValue - fromRangeMin) * newRange) / oldRange) + toRangeMin;
    }
}
