/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.glomadrian.pathloading.slice;

import com.github.glomadrian.pathloading.ResourceTable;
import com.github.glomadrian.pathloading.adapter.MenuItemAdapter;
import com.github.glomadrian.pathloading.ui.determinate.DeterminateViewFraction;
import com.github.glomadrian.pathloading.ui.drag.DragViewFraction;
import com.github.glomadrian.pathloading.ui.home.HomeViewFraction;
import com.github.glomadrian.pathloading.ui.material.MaterialViewFraction;
import com.github.glomadrian.pathloading.ui.progress.ProgressFraction;
import com.github.glomadrian.pathloading.ui.twoway.TwoWayFraction;
import com.github.glomadrian.pathloading.utils.BaseFraction;
import com.github.glomadrian.pathloading.utils.MenuItem;
import com.github.glomadrian.pathloading.utils.SlideMenu;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.ability.fraction.FractionScheduler;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MainAbilitySlice
 *
 * @since 2021-07-01
 */
public class MainAbilitySlice extends AbilitySlice {
    private Component menuBtn;

    private int currentPage = 0;

    private SlideMenu mSlideMenu;
    private Map<Integer, BaseFraction> baseFractionMap;
    private HomeViewFraction homeViewFraction;
    private boolean falg;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(Color.getIntColor("#3399cc"));
        baseFractionMap = new HashMap<>();

        menuBtn = findComponentById(ResourceTable.Id_image_titleBar_menu_switch);
        menuBtn.setClickedListener(component -> mSlideMenu.open());

        mSlideMenu = (SlideMenu) findComponentById(ResourceTable.Id_slide_menu);
        mSlideMenu.setIgnoreComponents(new Component[]{menuBtn});
        List<MenuItem> list = new ArrayList<>();
        String[] itemNameArray = {
            "Road Runner Sample",
            "Drag",
            "Determinate",
            "Material",
            "Two way",
            "Progress Tracing"
        };
        for (int i = 0; i < itemNameArray.length; i++) {
            list.add(new MenuItem(itemNameArray[i]));
        }
        MenuItemAdapter menuItemAdapter = new MenuItemAdapter(this, list);
        Component menuContent = LayoutScatter.getInstance(this).parse(ResourceTable.Layout_slide_menu, null, false);
        ListContainer listContainer = (ListContainer) menuContent.findComponentById(ResourceTable.Id_menu_list);
        listContainer.setItemProvider(menuItemAdapter);
        listContainer.setLongClickable(false);
        mSlideMenu.initMenu(menuContent);
        listContainer.setItemClickedListener((listContainer1, component, position, l) -> {
            Component lastComponent = listContainer1.getComponentAt(currentPage);
            Element element = lastComponent.getBackgroundElement();
            ((Component) lastComponent.getTag()).setAlpha(0.5f);
            lastComponent.setBackground(null);
            Text titleText = (Text) lastComponent.findComponentById(ResourceTable.Id_title);
            titleText.setTextColor(new Color(Color.getIntColor(getString(ResourceTable.String_textColor))));
            component.setBackground(element);
            Text titleText1 = (Text) component.findComponentById(ResourceTable.Id_title);
            titleText1.setTextColor(new Color(Color.getIntColor(getString(ResourceTable.String_textClickColor))));
            ((Component) component.getTag()).setAlpha(1);
            mSlideMenu.close();
            currentPage = position;
            jumpFraction(currentPage);
        });
        jumpFraction(currentPage);
    }

    /**
     * jumpFraction
     *
     * @param tag
     */
    public void jumpFraction(int tag) {
        FractionManager mFractionManager = ((FractionAbility) getAbility()).getFractionManager();
        FractionScheduler fractionScheduler = mFractionManager.startFractionScheduler();
        BaseFraction fraction = baseFractionMap.get(tag);
        switch (tag) {
            case HomeViewFraction.TAG:
                falg = true;
                fraction = new HomeViewFraction(getContext(), mSlideMenu);
                homeViewFraction = (HomeViewFraction) fraction;
                break;
            case DragViewFraction.TAG:
                falg = false;
                fraction = new DragViewFraction();
                break;
            case DeterminateViewFraction.TAG:
                falg = false;
                fraction = new DeterminateViewFraction();
                break;
            case MaterialViewFraction.TAG:
                falg = false;
                fraction = new MaterialViewFraction();
                break;
            case TwoWayFraction.TAG:
                falg = false;
                fraction = new TwoWayFraction();
                break;
            case ProgressFraction.TAG:
                falg = false;
                fraction = new ProgressFraction();
                break;
            default:
                break;
        }
        homeViewFraction.setFlag(falg);
        baseFractionMap.put(tag, fraction);
        fractionScheduler.add(ResourceTable.Id_fraction_root, fraction);
        fractionScheduler.show(fraction);
        fractionScheduler.replace(ResourceTable.Id_fraction_root, fraction);
        fractionScheduler.submit();
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
