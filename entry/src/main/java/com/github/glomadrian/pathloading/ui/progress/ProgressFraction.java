package com.github.glomadrian.pathloading.ui.progress;

import com.github.glomadrian.pathloading.ResourceTable;
import com.github.glomadrian.pathloading.utils.BaseFraction;
import com.github.glomadrian.roadrunner.ProgressRoadRunner;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;

public class ProgressFraction extends BaseFraction implements Slider.ValueChangedListener, Component.ClickedListener {
    public static final int TAG = 5;
    private Slider seek;
    private ProgressRoadRunner roadRunner;
    private Button runButton;
    private Button stopButton;
    private AnimatorValue infiniteAnimator;
    private int mProgress;

    @Override
    public int getUiContent() {
        return ResourceTable.Layout_progress_view;
    }

    @Override
    public void initComponent() {
        roadRunner = (ProgressRoadRunner) mComponent.findComponentById(ResourceTable.Id_progressRunner);
        runButton = (Button) mComponent.findComponentById(ResourceTable.Id_runButton);
        stopButton = (Button) mComponent.findComponentById(ResourceTable.Id_stopButton);
        runButton.setClickedListener(this::onClick);
        stopButton.setClickedListener(this::onClick);
        seek = (Slider) mComponent.findComponentById(ResourceTable.Id_seek);
        seek.setValueChangedListener(this);
    }

    @Override
    public void requestDisallowInterceptTouchEvent(boolean enable) {

    }

    @Override
    public void onProgressUpdated(Slider slider, int progress, boolean fromUser) {
        mProgress = progress;
        if (roadRunner != null) {
            roadRunner.setProgress(progress);
        }
    }

    @Override
    public void onTouchStart(Slider slider) {
        if (infiniteAnimator != null) {
            if (infiniteAnimator.isRunning()){
                infiniteAnimator.stop();
            }
        }
    }

    @Override
    public void onTouchEnd(Slider slider) {

    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_runButton:
                if (infiniteAnimator == null) {
                    infiniteAnimator = new AnimatorValue();
                    infiniteAnimator.setDuration(1500);
                    infiniteAnimator.setLoopedCount(AnimatorValue.INFINITE);
                    infiniteAnimator.setCurveType(Animator.CurveType.CUBIC_BEZIER_STANDARD);
                    infiniteAnimator.setValueUpdateListener(new MoveAnimation());
                }
                infiniteAnimator.start();
                break;
            case ResourceTable.Id_stopButton:
                if (infiniteAnimator != null)
                    infiniteAnimator.end();
                break;
        }
    }

    private class MoveAnimation implements AnimatorValue.ValueUpdateListener {
        @Override
        public void onUpdate(AnimatorValue animatorValue, float v) {
            roadRunner.setProgress((int)(mProgress + (100-mProgress) * v));
        }
    }
}
