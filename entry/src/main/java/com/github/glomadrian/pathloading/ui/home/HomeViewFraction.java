package com.github.glomadrian.pathloading.ui.home;

import com.github.glomadrian.pathloading.ResourceTable;
import com.github.glomadrian.pathloading.utils.BaseFraction;
import com.github.glomadrian.pathloading.utils.SlideMenu;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import com.github.glomadrian.roadrunner.DeterminateRoadRunner;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.app.Context;

public class HomeViewFraction extends BaseFraction {
    public static final int TAG = 0;
    private DeterminateRoadRunner determinateLoadingPath;
    private Image textImage;
    private TextAnimation textAnimation;
    private AnimatorValue progressAnimator;
    public SlideMenu slideMenu;
    private boolean flag;

    /**
     * HomeViewFraction(Context context, SlideMenu slideMenu)构造方法
     *
     * @param context
     * @param slideMenu
     */
    public HomeViewFraction(Context context, SlideMenu slideMenu) {
        mContext = context;
        this.slideMenu = slideMenu;
    }
    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public int getUiContent() {
        System.out.println("HomeViewFraction getUiContent :");
        return ResourceTable.Layout_home_view;
    }

    @Override
    public void initComponent() {
        determinateLoadingPath =  (DeterminateRoadRunner)mComponent.findComponentById(ResourceTable.Id_determinate);
        textImage = (Image) mComponent.findComponentById(ResourceTable.Id_text_image);
        initAnimation();
    }

    private void initAnimation() {
        textAnimation = new TextAnimation();
        textAnimation.setDuration(2000);
        textAnimation.setValueUpdateListener(textAnimation);
        textAnimation.setStateChangedListener(textAnimation);
        progressAnimator = new AnimatorValue();
        progressAnimator.setDuration(4000);
        progressAnimator.setDelay(2000);
        progressAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                int value = (int) (v * 1000);
                determinateLoadingPath.setValue(value);
            }
        });
        progressAnimator.setStateChangedListener(new Animator.StateChangedListener() {
            /**
             * onStart(Animator animator)
             *
             * @param animator
             */
            @Override
            public void onStart(Animator animator) {
            }

            /**
             * onStop(Animator animator)
             *
             * @param animator
             */
            @Override
            public void onStop(Animator animator) {

            }

            /**
             * onCancel(Animator animator)
             *
             * @param animator
             */
            @Override
            public void onCancel(Animator animator) {

            }

            /**
             * onEnd(Animator animator)
             *
             * @param animator
             */
            @Override
            public void onEnd(Animator animator) {
                determinateLoadingPath.stop();
                textAnimation.start();
            }

            /**
             * onPause(Animator animator)
             *
             * @param animator
             */
            @Override
            public void onPause(Animator animator) {

            }

            /**
             * onResume(Animator animator)
             *
             * @param animator
             */
            @Override
            public void onResume(Animator animator) {

            }
        });
        progressAnimator.start();
    }

    /**
     * onStart(Intent intent)
     *
     * @param intent
     */
    protected void onStart(Intent intent) {
    }

    /**
     * onActive()
     */
    protected void onActive() {
    }
    /**
     * onInactive()
     */
    protected void onInactive() {
    }

    /**
     * onForeground
     *
     * @param intent
     */
    protected void onForeground(Intent intent) {
    }

    /**
     * onBackground
     */
    protected void onBackground() {
    }

    /**
     * onStop()
     */
    protected void onStop() {
    }

    /**
     * onComponentDetach()
     */
    protected void onComponentDetach() {
    }


    /**
     * requestDisallowInterceptTouchEvent
     *
     * @param enable
     */
    @Override
    public void requestDisallowInterceptTouchEvent(boolean enable) {

    }

    private class TextAnimation extends AnimatorValue implements AnimatorValue.ValueUpdateListener, Animator.StateChangedListener {
        @Override
        public void onUpdate(AnimatorValue animatorValue, float v) {
            textImage.setAlpha(v);
        }

        @Override
        public void onStart(Animator animator) {
            textImage.setAlpha(0);
            textImage.setVisibility(Component.VISIBLE);
        }

        @Override
        public void onStop(Animator animator) {

        }

        @Override
        public void onCancel(Animator animator) {

        }

        @Override
        public void onEnd(Animator animator) {
           if (isFlag()) {
               slideMenu.open();
           }
        }

        @Override
        public void onPause(Animator animator) {

        }

        @Override
        public void onResume(Animator animator) {

        }
    }

}
