/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.glomadrian.pathloading.utils;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

import java.math.BigDecimal;

/**
 * 回弹效果helper类
 *
 * @since 2021-06-09
 */
public class ReboundHelper implements AnimatorValue.ValueUpdateListener, Animator.StateChangedListener {
    private static final int DEFAULTDURATION = 200;

    private ReboundOverListener mListener;

    private AnimatorValue mAnimatorValue;

    private float mFromX;
    private float mFromY;
    private float mToX;
    private float mToY;

    /**
     * ReboundHelper构造方法
     *
     * @param listener
     */
    public ReboundHelper(ReboundOverListener listener) {
        mListener = listener;
        mAnimatorValue = new AnimatorValue();
        mAnimatorValue.setDuration(DEFAULTDURATION);
        mAnimatorValue.setCurveType(Animator.CurveType.ACCELERATE);
        mAnimatorValue.setValueUpdateListener(this::onUpdate);
        mAnimatorValue.setStateChangedListener(this);
    }

    /**
     * 回弹 x
     *
     * @param component
     * @param toX
     */
    public void reboundX(Component component, float toX) {
        rebound(component, toX, component.getTranslationY());
    }

    /**
     * 回弹 Y
     *
     * @param component
     * @param toY
     */
    public void reboundY(Component component, float toY) {
        rebound(component, component.getTranslationX(), toY);
    }

    /**
     * 回弹XY
     *
     * @param component 回弹组件
     * @param toX
     * @param toY
     */
    public void rebound(Component component, float toX, float toY) {
        rebound(component.getTranslationX(), component.getTranslationY(), toX, toY);
    }

    /**
     * 回弹
     *
     * @param fromX
     * @param fromY
     * @param toX
     * @param toY
     */
    public void rebound(float fromX, float fromY, float toX, float toY) {
        if (mAnimatorValue.isRunning()) {
            return;
        }
        mFromX = fromX;
        mFromY = fromY;
        mToX = toX;
        mToY = toY;
        mAnimatorValue.start();
    }

    /**
     * 回弹速率
     *
     * @param rate
     * @return ReboundHelper
     */
    public ReboundHelper duration(float rate) {
        mAnimatorValue.setDuration((long) (rate * DEFAULTDURATION));
        return this;
    }

    /**
     * 立即停止回弹
     */
    public void stop() {
        if (mAnimatorValue.isRunning()) {
            mAnimatorValue.stop();
        }
    }

    @Override
    public void onUpdate(AnimatorValue animatorValue, float value) {
        float offsetX = new BigDecimal(mFromX).add((new BigDecimal(mToX).subtract(new BigDecimal(mFromX)))
            .multiply(new BigDecimal(value))).floatValue();
        float offsetY = new BigDecimal(mFromY).add((new BigDecimal(mToY).subtract(new BigDecimal(mFromY)))
            .multiply(new BigDecimal(value))).floatValue();
        mListener.update(offsetX, offsetY);
    }

    @Override
    public void onStart(Animator animator) {
    }

    @Override
    public void onStop(Animator animator) {
    }

    @Override
    public void onCancel(Animator animator) {
    }

    @Override
    public void onEnd(Animator animator) {
        mAnimatorValue.setDuration(DEFAULTDURATION);
        mListener.over(mToX, mToY);
    }

    @Override
    public void onPause(Animator animator) {
    }

    @Override
    public void onResume(Animator animator) {
    }

    /**
     * 回弹监听
     *
     * @since 2021-06-09
     */
    public interface ReboundOverListener {
        /**
         * 更新位置
         *
         * @param offsetX
         * @param offsetY
         */
        void update(float offsetX, float offsetY);

        /**
         * 回弹结束
         *
         * @param toX
         * @param toY
         */
        void over(float toX, float toY);
    }
}
