package com.github.glomadrian.pathloading.ui.drag;

import com.github.clans.fab.FloatingActionButton;
import com.github.glomadrian.pathloading.ResourceTable;
import com.github.glomadrian.pathloading.ui.MediaFavButton;
import com.github.glomadrian.pathloading.utils.BaseFraction;
import com.github.glomadrian.pathloading.utils.RangeUtils;
import com.github.glomadrian.roadrunner.DeterminateRoadRunner;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.element.VectorElement;
import ohos.agp.window.service.Window;
import ohos.agp.window.service.WindowManager;
import ohos.multimodalinput.event.TouchEvent;

public class DragViewFraction extends BaseFraction implements Component.ClickedListener {
    public static final int TAG = 1;
    private DeterminateRoadRunner determinateLoadingPath;
    private FloatingActionButton fabBtn;
    private FloatingActionButton fabBtn_square;
    @Override
    public int getUiContent() {
        return ResourceTable.Layout_drag_view;
    }

    @Override
    public void initComponent() {
        determinateLoadingPath =  (DeterminateRoadRunner)mComponent.findComponentById(ResourceTable.Id_determinate);
        fabBtn = (FloatingActionButton) mComponent.findComponentById(ResourceTable.Id_fabBtn);
        fabBtn_square = (FloatingActionButton) mComponent.findComponentById(ResourceTable.Id_fabBtnSquare);
        fabBtn.setClickedListener(this::onClick);
        fabBtn_square.setClickedListener(this::onClick);
        mComponent.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                float x1 = touchEvent.getPointerPosition(touchEvent.getIndex()).getX();
                float y1 =touchEvent.getPointerPosition(touchEvent.getIndex()).getY();
                switch (touchEvent.getAction()) {
                    case TouchEvent.POINT_MOVE:
                        float animationPosition =
                            RangeUtils.getFloatValueInRange(0, mComponent.getHeight(), 0f, 1f, y1);
                        float animationValue = RangeUtils.getFloatValueInRange(0, mComponent.getWidth(), 0, 100, x1);
                        System.out.println("getHeight-----------------" + mComponent.getHeight() + "width------------" + mComponent.getWidth()
                        + "animationPosition--------" + animationPosition + "animationValue----" + animationValue);
                        determinateLoadingPath.setPosition(animationPosition);
                        determinateLoadingPath.setValue((int) animationValue);
                        return true;
                }
                return true;
            }
        });
    }

    @Override
    public void requestDisallowInterceptTouchEvent(boolean enable) {

    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()){
            case ResourceTable.Id_fabBtn:
                fabBtn_square.setVisibility(Component.VISIBLE);
                fabBtn.setVisibility(Component.HIDE);
                determinateLoadingPath.start();
                break;
            case ResourceTable.Id_fabBtnSquare:
                fabBtn.setVisibility(Component.VISIBLE);
                fabBtn_square.setVisibility(Component.HIDE);
                determinateLoadingPath.stop();
                break;
        }
    }

    private float getTouchY(TouchEvent touchEvent, int index, Component component) {
        double touchY = 0;
        if (touchEvent.getPointerCount() > index) {
            touchY = touchEvent.getPointerPosition(index).getY();
        }
        return (float) touchY;
    }

    private float getTouchX(TouchEvent touchEvent, int index, Component component) {
        double touchX = 0;
        if (touchEvent.getPointerCount() > index) {
            touchX = touchEvent.getPointerPosition(index).getX();
        }
        return (float) touchX;
    }
}
