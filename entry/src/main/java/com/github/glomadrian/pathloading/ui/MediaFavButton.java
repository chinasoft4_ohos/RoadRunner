package com.github.glomadrian.pathloading.ui;

import com.github.glomadrian.pathloading.ResourceTable;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.VectorElement;
import ohos.app.Context;

public class MediaFavButton extends Image {
    private boolean isInPlayMode = true;
    private MediaFavButtonListener mediaFavButtonListener;

    /**
     * MediaFavButton
     *
     * @param context
     */
    public MediaFavButton(Context context) {
        super(context);
        initButton();
    }

    /**
     * MediaFavButton
     *
     * @param context
     * @param attrSet
     */
    public MediaFavButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initButton();
    }

    /**
     * MediaFavButton
     *
     * @param context
     * @param attrSet
     * @param styleName
     */
    public MediaFavButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initButton();
    }


    private void initButton() {
        setPixelMap(ResourceTable.Media_arrows);
        this.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (isInPlayMode) {
                    if (mediaFavButtonListener != null) {
                        mediaFavButtonListener.onPlayAction();
                    }
                    setPixelMap(ResourceTable.Media_square);
                    isInPlayMode = false;
                } else {
                    if (mediaFavButtonListener != null) {
                        mediaFavButtonListener.onStopAction();
                    }
                    setPixelMap(ResourceTable.Media_arrows);
                    isInPlayMode = true;
                }
            }
        });
    }

    public void setMediaFavButtonListener(
            MediaFavButtonListener mediaFavButtonListener) {
        this.mediaFavButtonListener = mediaFavButtonListener;
    }

    public interface MediaFavButtonListener {
        /**
         * onPlayAction
         */
        void onPlayAction();
        /**
         * onStopAction
         */
        void onStopAction();
    }
}
