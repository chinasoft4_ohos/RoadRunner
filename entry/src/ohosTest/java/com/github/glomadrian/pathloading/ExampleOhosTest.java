/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.github.glomadrian.pathloading;

import com.github.glomadrian.pathloading.slice.MainAbilitySlice;
import com.github.glomadrian.pathloading.ui.drag.DragViewFraction;
import com.github.glomadrian.pathloading.ui.home.HomeViewFraction;
import com.github.glomadrian.pathloading.utils.MenuItem;
import com.github.glomadrian.pathloading.utils.RangeUtils;
import com.github.glomadrian.pathloading.utils.ReboundHelper;
import com.github.glomadrian.pathloading.utils.SlideMenu;
import com.github.glomadrian.roadrunner.utils.AssertUtils;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.components.AttrSet;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    /**
     * testBundleName
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.github.glomadrian.pathloading", actualBundleName);
    }
    /**
     * testRangeUtils
     */
    @Test
    public void testRangeUtils()  {
        int fromRangeMin = 1;
        int fromRangeMax = 9;
        int toRangeMin = 3;
        int toRangeMax = 10;
        int fromRangeValue = 8;
        int result = RangeUtils.getIntValueInRange(fromRangeMin,fromRangeMax,toRangeMin,toRangeMax,fromRangeValue);
        System.out.println("testRangeUtils value :"+result);

    }
    /**
     * testRangeUtilsFloat
     */
    @Test
    public void testRangeUtilsFloat()  {
        float fromRangeMin = 2f;
        float fromRangeMax = 9f;
        float toRangeMin = 3f;
        float toRangeMax = 10f;
        float fromRangeValue = 8f;
        float floatValueInRange = RangeUtils.getFloatValueInRange(fromRangeMin,fromRangeMax,toRangeMin,toRangeMax,fromRangeValue);
        System.out.println("testRangeUtils float value :"+floatValueInRange);
    }
     /**
     * testAssertUtils
     */
    @Test
    public void testAssertUtils()  {
        MenuItem menu = new MenuItem("two way");
        String menuName = menu.getmItemName();
        System.out.println("getmItemName value :"+menuName);
    }



}